=========================================
Collections used with the signing service
=========================================

.. _collection-suite-signing-keys:

Category ``debian:suite-signing-keys``
======================================

This collection configures the signing keys that are suitable for signing a
:ref:`suite <collection-suite>` or for signing particular packages in it.

* Variables when adding items:

  * ``source_package_name``: the source package name that this key is
    restricted to

* Data: none

* Valid items:

  * ``debusine:signing-key`` artifacts

* Per-item data:

  * ``purpose``: the purpose of this key (copied from underlying artifact
    for ease of lookup)
  * ``source_package_name``: the source package name that this key is
    restricted to, if any (note that a single key may be added multiple
    times for different packages)

* Lookup names:

  * ``key:PURPOSE``: the key with the given ``purpose`` and no
    ``source_package_name``, if any
  * ``key:PURPOSE_SOURCE``: the key with the given ``purpose`` and either no
    ``source_package_name`` or one that equals ``SOURCE``, if any (for
    example, ``key:uefi_grub2`` would return a key suitable for making UEFI
    signatures of files produced by the ``grub2`` source package in this
    suite)

* Constraints:

  * there may be at most one key with a given purpose and source package
    name (or lack of one) active in the collection at a given time
