===================================
Tasks used with the signing service
===================================

.. _task-generate-key:

GenerateKey task
================

This is a :ref:`signing task <signing-app>` that generates a new key on a
signing worker and stores it for later use.

The ``task_data`` for this task may contain the following keys:

* ``purpose`` (required): the purpose of the key to generate: ``uefi`` or
  ``kmod``
* ``description`` (required): A text string with a human-readable
  description of the new key's intended purpose.

The output will be provided as a :ref:`debusine:signing-key
<artifact-signing-key>` artifact.  The task should typically be configured
with an :ref:`action-update-collection-with-artifacts` event reaction to
attach the new signing key to the collection where it will be used, such as
:ref:`debian:suite-signing-keys <collection-suite-signing-keys>`.

.. todo::

   This will need additional parameters once we start supporting HSMs.

.. _task-extract-for-signing:

ExtractForSigning task
======================

This is a worker task that takes the output of the :ref:`Sbuild task
<task-sbuild>` and extracts :ref:`debusine:signing-input
<artifact-signing-input>` artifacts from them for use by the :ref:`Sign task
<task-sign>`.

The ``task_data`` for this task may contain the following keys:

* ``input`` (required): a dictionary describing the input data:

  * ``template_artifact`` (:ref:`lookup-single`, required): a
    ``debian:binary-package`` artifact containing a `template package
    <https://wiki.debian.org/SecureBoot/Discussion#Source_template_inside_a_binary_package>`
  * ``binary_artifacts`` (:ref:`lookup-multiple`, required): a list of
    ``debian:binary-package`` artifacts used to find the packages referred
    to by the template's ``files.json``

* ``environment`` (:ref:`lookup-single` with default category
  ``debian:environments``, required): ``debian:system-tarball`` artifact
  that will be used to unpack binary packages using the ``unshare`` backend

The task operates as follows:

* It extracts the
  ``/usr/share/code-signing/$binary_package_name/files.json`` file from the
  template binary package.
* It checks that ``files.json`` uses only relative paths with no ``..``
  components.
* For each package in the template's ``files.json``:

  * It checks that the package name is a syntactically-valid Debian package
    name.
  * It finds the corresponding package among the binary artifacts.
  * If there is a ``trusted_certs`` entry, it copies it into the
    corresponding output artifact.
  * For each file:

    * It checks that the file name uses only relative paths with no ``..``
      components, and that the resulting path within the extracted binary
      package does not traverse symlinks to outside the extracted binary
      package.
    * It stores a copy of the file in the output artifact with the name
      ``$package/$file``.

The output will be provided as :ref:`debusine:signing-input
<artifact-signing-input>` artifacts, one for each package in the template's
``files.json``, with each artifact having a ``relates-to`` relationship to
the template package and to the binary package from which its files were
extracted.

.. _task-sign:

Sign task
=========

This is a :ref:`signing task <signing-app>` that signs the contents of a
:ref:`debusine:signing-input <artifact-signing-input>` artifact on a signing
worker.

The ``task_data`` for this task may contain the following keys:

* ``purpose`` (required): the purpose of the key to sign with: ``uefi`` or
  ``kmod`` (needed separately from ``key`` so that the scheduler can check
  whether the worker has the necessary tools available)
* ``unsigned`` (:ref:`lookup-single`, required): the
  ``debusine:signing-input`` artifact whose contents should be signed
* ``key`` (:ref:`lookup-single`, required): the ``debusine:signing-key``
  artifact to sign with; must match ``purpose``

The output will be provided as a :ref:`debusine:signing-output
<artifact-signing-output>` artifact.

.. _task-assemble-signed-source:

AssembleSignedSource task
=========================

This is a worker task that takes :ref:`debusine:signing-output
<artifact-signing-output>` artifacts produced by :ref:`Sign tasks
<task-sign>` and assembles the resulting source package.

The ``task_data`` for this task may contain the following keys:

* ``environment`` (:ref:`lookup-single` with default category
  ``debian:environments``, required): ``debian:system-tarball`` artifact
  that will be used to pack the source package using the ``unshare``
  backend.  ``dpkg-dev`` will be installed there if necessary.
* ``template`` (:ref:`lookup-single`, required): a
  :ref:`debian:binary-package <artifact-binary-package>` artifact containing
  a `source template
  <https://wiki.debian.org/SecureBoot/Discussion#Source_template_inside_a_binary_package>`_
* ``signed`` (:ref:`lookup-multiple`, required): signed
  ``debusine:signing-output`` artifacts matching the template

The task operates as follows:

* It makes a copy of the
  ``/usr/share/code-signing/$binary_package_name/source-template/``
  directory from the template binary package.
* It checks that ``debian/source/format`` is exactly ``3.0 (native)`` and
  that neither ``debian/source/options`` nor ``debian/source/local-options``
  exists.
* It checks that ``files.json`` uses only relative paths with no ``..``
  components.
* For each package name and file name in the template's ``files.json``, it
  finds the corresponding file in the signed artifacts and copies it into
  ``debian/signatures/$package/$file.sig``.  For this to work, the names of
  the files in the ``debusine:signing-input`` and
  ``debusine:signing-output`` artifacts must be composed of the binary
  package name, followed by ``/``, followed by the path in the corresponding
  ``file`` key in ``files.json``.
* It packs the resulting assembled source package using ``dpkg-source -b``,
  and makes a suitable ``.changes`` file for it using ``dpkg-genchanges``.

The output will be provided as a :ref:`debian:source-package
<artifact-source-package>` artifact, with a ``built-using`` relationship to
the :ref:`debian:binary-package <artifact-binary-package>` artifacts that
were related to the :ref:`input to the Sign task <artifact-signing-input>`,
and a :ref:`debian:upload <artifact-upload>` artifact containing that source
package and the corresponding ``.changes`` file.
