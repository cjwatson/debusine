=======================================
Artifacts used with the signing service
=======================================

.. _artifact-signing-key:

Category ``debusine:signing-key``
=================================

This artifact records the existence of a key in the signing service.  To
avoid expiry, these artifacts should also be added to the collection that
they are intended to be used with; they are therefore valid items in
:ref:`debian:suite-signing-keys <collection-suite-signing-keys>`
collections.

* Data:

  * ``purpose``: the purpose of this key: ``uefi`` or ``kmod``
  * ``fingerprint``: the fingerprint of this key
  * ``public_key``: the base64-encoded public key

* Files: none

* Relationships: none

.. todo::

   We'll eventually need to deal with removing private keys from the signing
   service when they're no longer referenced by anything in the debusine
   database, but it's not clear exactly what the lifetime rules should be.  See
   https://salsa.debian.org/freexian-team/debusine/-/merge_requests/616#note_497019.

.. _artifact-signing-input:

Category ``debusine:signing-input``
===================================

This artifact provides input to a :ref:`Sign task <task-sign>`.  It will
typically be created by the :ref:`Sbuild task <task-sbuild>`.

* Data:

  * ``trusted_certs``: a list of SHA-256 fingerprints of certificates built
    into the signed code as roots of trust for verifying additional
    privileged code (see `Describing the trust chain
    <https://wiki.debian.org/SecureBoot/Discussion#Describing_the_trust_chain>`_).
    If present, all the listed fingerprints must be listed in the
    ``DEBUSINE_SIGNING_TRUSTED_CERTS`` Django setting.  This is used to
    avoid accidentally creating trust chains from production to test signing
    certificates.

* Files: one or more files to be signed

* Relationships:

  * ``relates-to``: any other artifacts from which the files to be signed
    were extracted

.. _artifact-signing-output:

Category ``debusine:signing-output``
====================================

This artifact contains the output of a :ref:`Sign task <task-sign>`.

* Data:

  * ``purpose``: the purpose of the key used to sign these files: ``uefi``
    or ``kmod``
  * ``fingerprint``: the fingerprint of the key used to sign these files
  * ``results``: a list of dictionaries describing signed files, as follows
    (exactly one of ``output_file`` and ``error_message`` must be present):

    * ``file``: name of the file that was signed
    * ``output_file``: name of the file containing the signature
    * ``error_message``: error message resulting from attempting to sign
      the file

* Files:

  * zero or more files containing signatures

* Relationships:

  * ``relates-to``: the :ref:`debusine:signing-key <artifact-signing-key>`
    used for signing, and the corresponding :ref:`debusine:signing-input
    <artifact-signing-input>` artifact
