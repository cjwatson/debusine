.. _documentation-practices:

=======================
Documentation practices
=======================

The documentation is written following the `Diátaxis <https://diataxis.fr/>`_
principles.

Voice of the documentation
--------------------------

Tutorials and How-to guides refer to the user as "you": the documentation
tell the user how to do something on their own. For example `"You can use
Debian bookworm to install..."`.

For explanations and reference use impersonal and passive forms. For example
`"Debian bookworm can be used..."`, "Execution of the command results in..."`,
`"The command outputs..."` or `"Output is procued by the command"`.
