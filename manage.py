#!/usr/bin/env python3

# Copyright 2019, 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Command-line utility for administrative tasks."""

from debusine.__main__ import main


if __name__ == '__main__':
    main()
