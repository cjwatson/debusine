#  Copyright 2022 The Debusine Developers
#  See the AUTHORS file at the top-level directory of this distribution
#
#  This file is part of Debusine. It is subject to the license terms
#  in the LICENSE file found in the top-level directory of this
#  distribution. No part of Debusine, including this file, may be copied,
#  modified, propagated, or distributed except according to the terms
#  contained in the LICENSE file.

"""Package with test-helper code and utilities."""

import os

from debusine.test.base import TestCase

if os.environ.get("DJANGO_SETTINGS_MODULE") == "debusine.project.settings":
    # Relative import needed to break an import loop
    from .django import TestCase as DjangoTestCase  # noqa: ABS101

    class TestHelpersMixin(DjangoTestCase):
        """
        Compatibility class.

        This is needed for test cases still inheriting from TestHelperMixin.
        """

        # TODO: raise deprecationwarning when used?

else:

    class TestHelpersMixin(TestCase):  # type: ignore
        """
        Compatibility class.

        This is needed for test cases still inheriting from TestHelperMixin.
        """

        # TODO: raise deprecationwarning when used?
