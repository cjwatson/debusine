"""
The settings in this file are tailored overrides for running in production.

When running in production, selected.py should point to this file.
"""

# PostgreSQL should be used in production
from debusine.signing.settings.db_postgresql import DATABASES  # noqa: F401

# Use paths from the package
from debusine.signing.settings.pkg_paths import *  # noqa: F401, F403, I202
