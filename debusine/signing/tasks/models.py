# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Models used by debusine signing tasks."""

from debusine.artifacts.models import KeyPurpose
from debusine.tasks.models import (
    BaseDynamicTaskData,
    BaseTaskData,
    LookupSingle,
)


class SigningNoopData(BaseTaskData):
    """In-memory task data for the SigningNoop task."""

    result: bool = False


class GenerateKeyData(BaseTaskData):
    """In-memory task data for the GenerateKey task."""

    purpose: KeyPurpose
    description: str


class SignData(BaseTaskData):
    """In-memory task data for the Sign task."""

    purpose: KeyPurpose
    unsigned: LookupSingle
    key: LookupSingle


class SignDynamicData(BaseDynamicTaskData):
    """Dynamic data for the Sign task."""

    unsigned_id: int
    key_id: int
