# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Server-side database interaction for worker tasks.

Worker tasks mostly run on external workers, but they also run some
pre-dispatch code on the server which needs to access the database.  To
avoid having to import Django from :py:mod:`debusine.tasks`, we define an
interface that is implemented by server code.
"""

from abc import ABC, abstractmethod

from debusine.artifacts.models import CollectionCategory
from debusine.tasks.models import LookupMultiple, LookupSingle


class TaskDatabaseInterface(ABC):
    """Interface for interacting with the database from worker tasks."""

    @abstractmethod
    def lookup_single_artifact(
        self,
        lookup: LookupSingle,
        default_category: CollectionCategory | None = None,
    ) -> int:
        """Look up a single artifact using :ref:`lookup-single`."""

    @abstractmethod
    def lookup_multiple_artifacts(
        self,
        lookup: LookupMultiple,
        default_category: CollectionCategory | None = None,
    ) -> list[int]:
        """Look up multiple artifacts using :ref:`lookup-multiple`."""
