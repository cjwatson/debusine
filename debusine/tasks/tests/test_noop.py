# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the noop task."""

from unittest import TestCase

from debusine.tasks import TaskConfigError
from debusine.tasks.noop import Noop


class NoopTaskTests(TestCase):
    """Test the noop task."""

    def test_execute_true(self):
        """Assert execute() return True (result is True)."""
        noop = Noop({"result": True})
        self.assertTrue(noop.execute())

    def test_execute_false(self):
        """Assert execute() return False (result is False)."""
        noop = Noop({"result": False})
        self.assertFalse(noop.execute())

    def test_no_additional_properties(self):
        """Assert no additional properties."""
        error_msg = "extra fields not permitted"
        with self.assertRaisesRegex(TaskConfigError, error_msg):
            Noop({"result": "True", "input": {}})
