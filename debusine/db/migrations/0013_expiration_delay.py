# Generated by Django 3.2.19 on 2024-02-06 02:54

import datetime
from django.db import migrations, models
from django.db.models import F, Value
from django.db.models.functions import Cast, Concat, ExtractDay
import datetime


def workspace_day_to_duration(apps, schema_editor):
    Workspace = apps.get_model("db", "Workspace")
    Workspace.objects.update(
        default_expiration_delay=Cast(
            Concat(
                Cast(
                    F("default_expiration_delay_old"),
                    output_field=models.CharField(),
                ),
                Value(" day"),
            ),
            output_field=models.DurationField(),
        )
    )


def artifact_expire_at_to_expiration_delay(apps, schema_editor):
    Artifact = apps.get_model("db", "Artifact")
    Artifact.objects.filter(expire_at__isnull=False).update(
        expiration_delay=F("expire_at") - F("created_at")
    )


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0012_workspace_default_expiration_delay'),
    ]

    operations = [
        migrations.AddField(
            model_name='artifact',
            name='expiration_delay',
            field=models.DurationField(blank=True, null=True),
        ),
        migrations.RunPython(artifact_expire_at_to_expiration_delay),
        migrations.RemoveField(
            model_name='artifact',
            name='expire_at',
        ),
        migrations.RenameField(
            model_name='workspace',
            old_name='default_expiration_delay',
            new_name='default_expiration_delay_old',
        ),
        migrations.AddField(
            model_name='workspace',
            name='default_expiration_delay',
            field=models.DurationField(
                default=datetime.timedelta(0),
                help_text='minimal time that a new artifact is kept in the workspace before being expired',
            ),
        ),
        migrations.RunPython(workspace_day_to_duration),
        migrations.RemoveField(
            model_name='workspace',
            name='default_expiration_delay_old',
        ),
    ]
