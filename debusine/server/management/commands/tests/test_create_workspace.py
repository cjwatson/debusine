# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the management command create_workspace."""

from datetime import timedelta

from django.core.management import CommandError
from django.test import TestCase

from debusine.db.models import DEFAULT_WORKSPACE_NAME, FileStore, Workspace
from debusine.server.management.commands.tests import call_command


class CreateWorkspaceCommandTests(TestCase):
    """Tests for the create_workspace command."""

    def test_create_workspace(self):
        """create_workspace creates a new workspace."""
        name = "test"
        stdout, stderr, exit_code = call_command('create_workspace', name)

        workspace = Workspace.objects.get(name=name)

        self.assertFalse(workspace.public)
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

    def test_create_public_workspace(self):
        """create_workspace creates a new public workspace."""
        name = "test"
        stdout, stderr, exit_code = call_command(
            'create_workspace', name, public=True
        )

        workspace = Workspace.objects.get(name=name)

        self.assertTrue(workspace.public)
        self.assertEqual(workspace.default_expiration_delay, timedelta(0))
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

    def test_create_workspace_with_default_expiration_delay(self):
        """Creates a workspace with a default_expiration_delay."""
        name = "test"
        default_expiration_delay = timedelta(days=7)
        stdout, stderr, exit_code = call_command(
            'create_workspace',
            name,
            default_expiration_delay=default_expiration_delay,
        )

        workspace = Workspace.objects.get(name=name)

        self.assertFalse(workspace.public)
        self.assertEqual(
            workspace.default_expiration_delay, default_expiration_delay
        )
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

    def test_create_workspace_with_default_file_store(self):
        """Create a workspace with a different default file store."""
        name = "test"
        default_file_store = FileStore.objects.create(
            name="different", backend=FileStore.BackendChoices.LOCAL
        )
        stdout, stderr, exit_code = call_command(
            "create_workspace", name, default_file_store="different"
        )

        workspace = Workspace.objects.get(name=name)

        self.assertFalse(workspace.public)
        self.assertEqual(workspace.default_file_store, default_file_store)
        self.assertEqual(stderr, "")
        self.assertEqual(exit_code, 0)

    def test_create_workspace_with_default_file_store_not_found(self):
        """create_workspace: default file store not found."""
        with self.assertRaisesRegex(
            CommandError, 'File store "nonexistent" not found'
        ) as exc:
            call_command(
                "create_workspace", "test", default_file_store="nonexistent"
            )

        self.assertEqual(exc.exception.returncode, 3)

    def test_create_workspace_duplicated_name(self):
        """create_workspace raise CommandError."""
        expected_error = "A workspace with this name already exists"
        with self.assertRaisesRegex(CommandError, expected_error) as exc:
            call_command("create_workspace", DEFAULT_WORKSPACE_NAME)

        self.assertEqual(exc.exception.returncode, 3)
