# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine-admin command to list notification channels."""

from debusine.db.models import NotificationChannel
from debusine.django.management.debusine_base_command import DebusineBaseCommand
from debusine.server.management.utils import print_notification_channels


class Command(DebusineBaseCommand):
    """Command to list notification channels."""

    help = "List notification channels"

    def handle(self, *args, **options):
        """List notification channels."""
        notification_channels = NotificationChannel.objects.all()
        print_notification_channels(self.stdout, notification_channels)
        raise SystemExit(0)
