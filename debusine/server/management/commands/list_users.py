# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine-admin command to list work requests."""

from django.contrib.auth import get_user_model

from debusine.django.management.debusine_base_command import DebusineBaseCommand
from debusine.server.management.utils import print_users


class Command(DebusineBaseCommand):
    """Command to list work requests."""

    help = "List users sorted by date joined."

    def handle(self, *args, **options):
        """List users."""
        users = get_user_model().objects.filter(is_system=False)
        print_users(self.stdout, users)
        raise SystemExit(0)
