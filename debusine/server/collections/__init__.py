# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Collection managers.

Provides interfaces to interact with Collections: add items, remove,
lookups, etc.

Each specific CollectionManager implement their own business logic.
"""

from debusine.server.collections.base import (  # noqa: F401
    CollectionManagerInterface,
    ItemAdditionError,
    ItemRemovalError,
)
from debusine.server.collections.debian_environments import (  # noqa: F401
    DebianEnvironmentsManager,
)
from debusine.server.collections.debian_suite import (  # noqa: F401
    DebianSuiteManager,
)
from debusine.server.collections.debian_suite_lintian import (  # noqa: F401
    DebianSuiteLintianManager,
)
from debusine.server.collections.debian_suite_signing_keys import (  # noqa: E501, F401
    DebianSuiteSigningKeysManager,
)
from debusine.server.collections.workflow_internal import (  # noqa: F401
    WorkflowInternalManager,
)
