# Copyright 2021-2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""URLs for the server application - API."""

from django.urls import path

from debusine.server.views.artifacts import (
    ArtifactRelationsView,
    ArtifactView,
    UploadFileView,
)
from debusine.server.views.lookups import (
    LookupMultipleView,
    LookupSingleView,
)
from debusine.server.views.work_requests import (
    WorkRequestRetryView,
    WorkRequestView,
)
from debusine.server.views.workers import (
    GetNextWorkRequestView,
    RegisterView,
    UpdateWorkRequestAsCompletedView,
    UpdateWorkerDynamicMetadataView,
)
from debusine.server.views.workflows import (
    WorkflowTemplateView,
    WorkflowView,
)

app_name = 'server'


urlpatterns = [
    path(
        '1.0/work-request/get-next-for-worker/',
        GetNextWorkRequestView.as_view(),
        name='work-request-get-next',
    ),
    path(
        '1.0/work-request/',
        WorkRequestView.as_view(),
        name='work-requests',
    ),
    path(
        '1.0/work-request/<int:work_request_id>/',
        WorkRequestView.as_view(),
        name='work-request-detail',
    ),
    path(
        '1.0/work-request/<int:work_request_id>/completed/',
        UpdateWorkRequestAsCompletedView.as_view(),
        name='work-request-completed',
    ),
    path(
        '1.0/work-request/<int:work_request_id>/retry/',
        WorkRequestRetryView.as_view(),
        name='work-requests-retry',
    ),
    path(
        "1.0/artifact/",
        ArtifactView.as_view(),
        name="artifact-create",
    ),
    path(
        "1.0/artifact/<int:artifact_id>/",
        ArtifactView.as_view(),
        name="artifact",
    ),
    path(
        "1.0/artifact/<int:artifact_id>/files/<path:file_path>/",
        UploadFileView.as_view(),
        name="upload-file",
    ),
    path(
        "1.0/lookup/single/", LookupSingleView.as_view(), name="lookup-single"
    ),
    path(
        "1.0/lookup/multiple/",
        LookupMultipleView.as_view(),
        name="lookup-multiple",
    ),
    path('1.0/worker/register/', RegisterView.as_view(), name='register'),
    path(
        '1.0/worker/dynamic-metadata/',
        UpdateWorkerDynamicMetadataView.as_view(),
        name='worker-dynamic-metadata',
    ),
    path(
        "1.0/artifact-relation/",
        ArtifactRelationsView.as_view(),
        name="artifact-relation-list",
    ),
    path(
        "1.0/artifact-relation/<int:pk>/",
        ArtifactRelationsView.as_view(),
        name="artifact-relation-detail",
    ),
    path(
        "1.0/workflow-template/",
        WorkflowTemplateView.as_view(),
        name="workflow-templates",
    ),
    path(
        "1.0/workflow-template/<int:pk>/",
        WorkflowTemplateView.as_view(),
        name="workflow-template-detail",
    ),
    path("1.0/workflow/", WorkflowView.as_view(), name="workflows"),
]
