# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Template tag library to render UI shortcuts."""

from django import template
from django.db.models import Model
from django.utils.safestring import SafeString

from debusine.web.views.ui_shortcuts import UIShortcut

register = template.Library()


@register.simple_tag
def ui_shortcuts(obj: Model) -> list[UIShortcut]:
    """Return the stored UI shortcuts for the given object."""
    stored = getattr(obj, "_ui_shortcuts", None)
    if stored is None:
        return []
    return stored


@register.simple_tag(takes_context=True)
def render_ui_shortcut(context, shortcut: UIShortcut) -> SafeString:
    """Render a UI shortcut."""
    return shortcut.render(context)
