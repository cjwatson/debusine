# Copyright 2023--2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""URLs related to workspaces."""

from django.urls import include, path

import debusine.web.views.workspace as views

app_name = "workspaces"

urlpatterns = [
    path(
        "",
        views.WorkspaceListView.as_view(),
        name="list",
    ),
    path(
        "<str:wname>/view/",
        views.WorkspaceDetailView.as_view(),
        name="detail",
    ),
    path(
        "<str:wname>/collection/",
        include(
            "debusine.web.urls.collections",
            namespace="collections",
        ),
    ),
]
