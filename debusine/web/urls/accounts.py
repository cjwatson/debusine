# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine login and accounts URLs Configuration."""
from django.contrib.auth import views as auth_views
from django.urls import path

from debusine.server.signon.views import (
    BindIdentityView,
    OIDCAuthenticationCallbackView,
)
from debusine.web.views import LogoutView

app_name = "accounts"

urlpatterns = [
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="account/login.html"),
        name="login",
    ),
    path(
        "logout/",
        LogoutView.as_view(
            template_name="account/logged_out.html",
            extra_context={"is_logout_view": True},
        ),
        name="logout",
    ),
    path(
        "accounts/oidc_callback/<name>/",
        OIDCAuthenticationCallbackView.as_view(),
        name="oidc_callback",
    ),
    path(
        "accounts/bind_identity/<name>/",
        BindIdentityView.as_view(),
        name="bind_identity",
    ),
]
