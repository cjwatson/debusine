# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Utility code for views development."""

from collections.abc import Callable
from typing import Any, TypeVar

_T = TypeVar("_T", bound=Callable[..., Any])


def ui_prototype(code: _T) -> _T:
    """Mark code as UI prototype."""
    return code
