# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the view utilities."""


from unittest import TestCase

from debusine.web.utils import ui_prototype


class TestUtils(TestCase):
    """Test view utilities."""

    def test_ui_prototype(self):
        """Check that it returns its argument."""
        self.assertIs(ui_prototype(TestCase), TestCase)
