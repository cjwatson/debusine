# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for account (log in, log out)."""

from django.test import TestCase
from django.urls import reverse

from rest_framework import status

from debusine.web.views.tests.utils import ViewTestMixin


class LoginTests(ViewTestMixin, TestCase):
    """Test login functionality and template."""

    def test_view(self):
        """Generic test of the view."""
        next_path = "/workers"
        response = self.client.get(
            reverse("accounts:login") + f"?next={next_path}"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Correct title
        self.assertContains(response, "Log in")

        # Labels for the form
        self.assertContains(response, "Username:")
        self.assertContains(response, "Password:")

        # Has "next" form hidden field
        tree = self.assertHTMLValid(response)

        input_next = tree.xpath(
            ".//form//input[@name='next' and @type='hidden']"
        )

        self.assertEqual(len(input_next), 1)

        self.assertEqual(input_next[0].attrib["value"], next_path)

    def test_login_form_errors(self):
        """If the form has errors: template renders them."""
        response = self.client.post(
            reverse("accounts:login"), {"username": "", "password": ""}
        )

        self.assertContains(
            response,
            "Your username and password didn't match. Please try again.",
        )
        self.assertTrue(response.context["form"].errors)


class LogoutTests(ViewTestMixin, TestCase):
    """Tests related to logout functionality."""

    def test_view_login_without_next(self):
        """
        _base.html do not include ?next=logout path.

        URL for accounts:logout add variable, _base.html use the variable
        to not add ?next={next_path}
        """
        response = self.client.get(reverse("accounts:logout"))

        next_path = reverse("accounts:logout")
        self.assertNotContains(response, f"?next={next_path}")

    def test_view_login_with_next(self):
        """_base.html do include ?next=workspaces_list path."""
        response = self.client.get(reverse("workspaces:list"))

        next_path = reverse("workspaces:list")
        self.assertContains(response, f"?next={next_path}")
