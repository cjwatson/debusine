# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""View extension to display contents of files."""

import contextlib
import io
import mimetypes
import mmap
import os.path
from collections.abc import Generator, Iterable, Iterator
from enum import StrEnum
from functools import cached_property
from pathlib import Path
from typing import Any, IO, NamedTuple, Self, TYPE_CHECKING

from django.core.exceptions import ImproperlyConfigured
from django.http import FileResponse
from django.http.response import HttpResponseBase
from django.shortcuts import redirect
from django.utils.safestring import SafeString
from django.views.generic.base import ContextMixin, View

import pygments
import pygments.formatters
import pygments.lexers

from rest_framework import status

from debusine.db.models import FileInArtifact
from debusine.server.views import ProblemResponse
from debusine.utils import parse_range_header

if TYPE_CHECKING:
    HtmlFormatter = pygments.formatters.HtmlFormatter
else:
    # pygments doesn't support generic types at run-time yet.
    class _HtmlFormatter:
        def __class_getitem__(*args):
            return pygments.formatters.HtmlFormatter

    HtmlFormatter = _HtmlFormatter


# Above this file size, only offer to view or download raw
MAX_FILE_SIZE = 2 * 1024 * 1024


class FileViewTypes(StrEnum):
    """Tags used to drive specialised views for files."""

    TEXT = "text"
    BINARY = "binary"
    TOO_BIG = "too_big"


class FileUI(NamedTuple):
    """Information about how to display a file."""

    #: content type to use for downloads
    content_type: str
    #: tag identifying specialised view functions to display the file
    view_type: FileViewTypes
    #: pygments lexer to use (default: autodetect from mimetype)
    pygments_lexer: str | None = None

    @classmethod
    def from_file_in_artifact(cls, file: FileInArtifact) -> Self:
        """Get a FileUI for a file."""
        # TODO: this is currently rather simple minded.
        # If it will need more complexity in the future,
        # debusine.artfacts.models.ArtifactData can be extended with a
        # get_content_type or get_file_ui method.
        # Another thing we may want to consider is adding a content_type
        # argument to File.
        content_type: str
        view_type: FileViewTypes
        pygments_lexer: str | None = None
        match os.path.splitext(file.path)[1]:
            case ".buildinfo" | ".dsc" | ".changes":
                content_type = "text/plain; charset=utf-8"
                view_type = FileViewTypes.TEXT
                pygments_lexer = "debcontrol"
            case ".txt" | ".log" | ".build" | ".buildlog" | ".sources":
                content_type = "text/plain; charset=utf-8"
                view_type = FileViewTypes.TEXT
            case '.md':
                content_type = "text/markdown; charset=utf-8"
                view_type = FileViewTypes.TEXT
            case _:
                # Logic taken from django FileResponse.set_headers
                encoding_map = {
                    'bzip2': 'application/x-bzip',
                    'gzip': 'application/gzip',
                    'xz': 'application/x-xz',
                }
                _content_type, encoding = mimetypes.guess_type(file.path)
                content_type = _content_type or 'application/octet-stream'
                # Encoding isn't set to prevent browsers from automatically
                # uncompressing files.
                if encoding:
                    content_type = encoding_map.get(encoding, content_type)
                view_type = FileViewTypes.BINARY

        if file.file.size > MAX_FILE_SIZE:
            view_type = FileViewTypes.TOO_BIG

        return cls(content_type, view_type, pygments_lexer)


class LinenoHtmlFormatter(HtmlFormatter[Any]):
    """HtmlFormatter that keeps a count of line numbers."""

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        """Keep a count of line numbers."""
        super().__init__(*args, **kwargs)
        self.line_count = 0

    def wrap(
        self, *args: Any, **kwargs: Any
    ) -> Generator[tuple[int, str], None, None]:
        """Count rendered lines."""
        for val in super().wrap(*args, **kwargs):
            self.line_count += val[0]
            yield val

    def render_linenos(self) -> SafeString:
        """Render line numbers to go alongside rendered contents."""
        width = len(str(self.line_count))
        return SafeString(
            "\n".join(
                f"<a id='L{n}' href='#L{n}'>{str(n).rjust(width)}</a>"
                for n in range(1, self.line_count + 1)
            )
        )


class FileView(ContextMixin):
    """View mixin to handle displaying file contents."""

    def get_file_in_artifact(self) -> FileInArtifact | None:
        """
        Return the FileInArtifact object to display.

        To optimize querying the FileInArtifact object, note that the view is
        likely to access:
        * `artifact`
        * `artifact.workspace`
        * `artifact.workspace.default_file_store`
        * `file`
        """
        raise ImproperlyConfigured(
            "please provide a get_file_in_artifact implementation"
        )

    @cached_property
    def file_in_artifact(self) -> FileInArtifact | None:
        """Cached version of get_file_in_artifact."""
        return self.get_file_in_artifact()

    def get_file_ui(self) -> FileUI | None:
        """Return the FileUI object to drive the display."""
        if self.file_in_artifact:
            return FileUI.from_file_in_artifact(self.file_in_artifact)
        else:
            return None

    @cached_property
    def file_ui(self) -> FileUI | None:
        """Cached version of get_file_ui."""
        return self.get_file_ui()

    @contextlib.contextmanager
    def _open_file(self) -> Iterator[IO[bytes]]:
        assert self.file_in_artifact is not None
        workspace = self.file_in_artifact.artifact.workspace
        file_backend = workspace.default_file_store.get_backend_object()
        with file_backend.get_stream(self.file_in_artifact.file) as stream:
            yield stream

    # add_context_data_* names match FileViewTypes enum options

    def add_context_data_text(self, context: dict[str, Any]) -> None:
        """Add context data for text files."""
        # Generate CSS with:
        # python3 -m pygments -S github-dark -f html \
        #     > debusine/web/static/web/css/debusine-code-highlight.css
        # See https://pygments.org/styles/ for a list of styles
        assert self.file_ui is not None
        if self.file_ui.pygments_lexer:
            lexer = pygments.lexers.get_lexer_by_name(
                self.file_ui.pygments_lexer
            )
        else:
            try:
                lexer = pygments.lexers.get_lexer_for_mimetype(
                    self.file_ui.content_type
                )
            except pygments.util.ClassNotFound:
                lexer = pygments.lexers.get_lexer_for_mimetype("text/plain")

        formatter = LinenoHtmlFormatter(
            cssclass="file_highlighted",
            linenos=False,
            # linenos="table",
            # lineanchors="L",
            # anchorlinenos=True,
        )

        with self._open_file() as stream:
            formatted = pygments.highlight(stream.read(), lexer, formatter)

        context["file_linenumbers"] = SafeString(formatter.render_linenos())
        context["file_content"] = SafeString(formatted)

    def add_context_data_binary(self, context: dict[str, Any]) -> None:
        """Add context data for binary files."""
        pass

    def add_context_data_too_big(self, context: dict[str, Any]) -> None:
        """Add context data for files too big to display."""
        context["file_max_size"] = MAX_FILE_SIZE

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Add context information to render a file."""
        context = super().get_context_data(**kwargs)
        if self.file_in_artifact is None:
            return context
        assert self.file_ui is not None
        context["file_in_artifact"] = self.file_in_artifact
        context["file_ui"] = self.file_ui
        context["file_template"] = f"web/_file_{self.file_ui.view_type}.html"
        func = getattr(self, f"add_context_data_{self.file_ui.view_type}", None)
        assert func is not None
        func(context)
        return context


class FileDownloadMixin(View):
    """File streaming functions for views."""

    def stream_file(
        self,
        file_in_artifact: FileInArtifact,
        ui_info: FileUI,
        download: bool = True,
    ) -> HttpResponseBase:
        """Return a response that streams the_given file."""
        try:
            content_range = parse_range_header(self.request.headers)
        except ValueError as exc:
            # It returns ProblemResponse because ranges are not used
            # by end users directly
            return ProblemResponse(str(exc))

        workspace = file_in_artifact.artifact.workspace
        file_backend = workspace.default_file_store.get_backend_object()

        # Ignore func-returns-value for now: LocalFileBackend.get_url always
        # returns None, but the backend might be something else in future.
        url = file_backend.get_url(
            file_in_artifact.file
        )  # type: ignore[func-returns-value]
        if url is not None:
            # The client can download the file from the backend
            # TODO: this does not allow to set content-disposition, that is, to
            # distinguish between "view raw" or "download". Not sure if it can
            # be solved.
            return redirect(url)

        with file_backend.get_stream(file_in_artifact.file) as file:
            file_size = file_in_artifact.file.size
            status_code: int
            if content_range is None:
                # Whole file
                status_code = status.HTTP_200_OK
                start = 0
                end = file_size - 1
            else:
                # Part of a file
                status_code = status.HTTP_206_PARTIAL_CONTENT
                start = content_range["start"]
                end = content_range["end"]

                # It returns ProblemResponse because ranges are not used
                # by end users directly
                if start > file_size:
                    return ProblemResponse(
                        f"Invalid Content-Range start: {start}. "
                        f"File size: {file_size}"
                    )

                elif end >= file_size:
                    return ProblemResponse(
                        f"Invalid Content-Range end: {end}. "
                        f"File size: {file_size}"
                    )

            # Use mmap:
            # - No support for content-range or file chunk in Django
            #   as of 2023, so create filelike object of the right chunk
            # - Prevents FileResponse.file_to_stream.name from taking
            #   precedence over .filename and break mimestype
            file_partitioned: Iterable[object]
            if file_size == 0:
                # cannot mmap an empty file
                file_partitioned = io.BytesIO(b"")
            else:
                file_partitioned = mmap.mmap(
                    file.fileno(), end + 1, prot=mmap.PROT_READ
                )
                file_partitioned.seek(start)

            filename = Path(file_in_artifact.path)

            response = FileResponse(
                file_partitioned,
                filename=filename.name,
                status=status_code,
            )

            response["Accept-Ranges"] = "bytes"
            response["Content-Length"] = end - start + 1
            if file_size > 0:
                response["Content-Range"] = f"bytes {start}-{end}/{file_size}"
            if download:
                disposition = "attachment"
            else:
                disposition = "inline"
            response["Content-Disposition"] = (
                f'{disposition}; filename="{filename.name}"'
            )
            response["Content-Type"] = ui_info.content_type

            return response


class PathMixin(View):
    """View that accepts a path kwarg."""

    @staticmethod
    def normalize_path(path: str) -> str:
        """
        Normalize a path used as a subdirectory prefix.

        It will also constrain paths not to point above themselves by extra ../
        components
        """
        path = os.path.normpath(path).strip("/")
        while path.startswith("../"):
            path = path[3:]
        if path in ("", ".", ".."):
            return "/"
        return f"/{path}/"

    @cached_property
    def path(self) -> str:
        """
        Return the current subdirectory as requested by the user.

        This returns a path relative to the root of the artifact, with ""
        standing for the whole artifact
        """
        if not (path := self.kwargs.get("path")):
            return ""
        return self.normalize_path(path)[1:]
