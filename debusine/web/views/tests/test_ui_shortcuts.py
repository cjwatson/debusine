# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the UIShortcuts base view."""

from typing import ClassVar

from django.urls import reverse

from debusine.db.models import Artifact, WorkRequest
from debusine.test.django import TestCase
from debusine.web.templatetags.ui_shortcuts import (
    ui_shortcuts as template_ui_shortcuts,
)
from debusine.web.views.ui_shortcuts import UIShortcut, UIShortcutsView


class TestUIShortcuts(TestCase):
    """Tests for UIShortcut."""

    work_request: ClassVar[WorkRequest]
    source: ClassVar[Artifact]
    buildlog: ClassVar[Artifact]

    @classmethod
    def setUpTestData(cls) -> None:
        """Set up test fixture."""
        super().setUpTestData()
        cls.work_request = cls.playground.create_work_request()
        cls.source = cls.playground.create_source_artifact()
        cls.buildlog = cls.playground.create_build_log_artifact(
            work_request=cls.work_request
        )

    def test_as_a(self) -> None:
        """Test rendering to HTML."""
        action = UIShortcut(label="LABEL", icon="ICON", url="URL")
        self.assertEqual(
            action.render(None),
            "<a class='btn btn-outline-secondary'"
            " href='URL' title='LABEL'>"
            "<span class='bi bi-ICON'></span>"
            "</a>",
        )

    def test_work_request_view(self) -> None:
        """Test create_work_request_view."""
        action = UIShortcut.create_work_request_view(self.work_request)
        self.assertEqual(action.label, "View work request")
        self.assertEqual(action.icon, "hammer")
        self.assertEqual(
            action.url,
            reverse(
                "work_requests:detail", kwargs={"pk": self.work_request.pk}
            ),
        )

    def test_artifact_view(self) -> None:
        """Test create_artifact_view."""
        action = UIShortcut.create_artifact_view(self.source)
        self.assertEqual(action, UIShortcut.create_artifact_view(self.source))

        action = UIShortcut.create_artifact_view(self.buildlog)
        self.assertEqual(action, UIShortcut.create_artifact_view(self.buildlog))

    def test_artifact_download(self) -> None:
        """Test create_artifact_download."""
        action = UIShortcut.create_artifact_download(self.source)
        self.assertEqual(action.label, "Download artifact")
        self.assertEqual(action.icon, "download")
        self.assertEqual(
            action.url,
            reverse(
                "artifacts:download", kwargs={"artifact_id": self.source.pk}
            )
            + "?archive=tar.gz",
        )

    def test_ui_shortcuts_in_context_data(self) -> None:
        """Test that UI shortcuts are added to context."""
        view = UIShortcutsView()
        actions = view.get_context_data()
        self.assertEqual(actions["main_ui_shortcuts"], [])

    def test_object_actions(self) -> None:
        """Test that stored object actions can be retrieved."""
        action1 = UIShortcut.create_artifact_view(self.buildlog)
        action2 = UIShortcut.create_artifact_download(self.buildlog)
        view = UIShortcutsView()
        view.add_object_shortcuts(self.buildlog, action1)
        view.add_object_shortcuts(self.buildlog, action2)
        self.assertEqual(
            template_ui_shortcuts(self.buildlog), [action1, action2]
        )
        self.assertEqual(template_ui_shortcuts(self.work_request), [])
