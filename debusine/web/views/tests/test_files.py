# Copyright 2022-2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the artifact views."""

import json
import os.path
import unittest
from typing import ClassVar, NamedTuple, cast
from unittest import mock

from django.core.exceptions import ImproperlyConfigured
from django.http.request import HttpHeaders
from django.http.response import HttpResponseBase
from django.utils.datastructures import CaseInsensitiveMapping
from django.views.generic.base import View

from rest_framework import status

from debusine.db.models import (
    File,
    FileInArtifact,
    FileStore,
)
from debusine.server.file_backend.interface import FileBackendInterface
from debusine.server.views import ProblemResponse
from debusine.test.django import TestCase
from debusine.web.views.files import (
    FileDownloadMixin,
    FileUI,
    FileView,
    FileViewTypes,
    MAX_FILE_SIZE,
    PathMixin,
)
from debusine.web.views.tests.utils import ViewTestMixin


class TestableFileView(FileView, View):
    """FileView that can be instantiated as a view."""

    pass


class FileUITests(unittest.TestCase):
    """Tests for the FileUI class."""

    def test_from_file_in_artifact(self) -> None:
        """Test from_file_in_artifact."""

        class SubTest(NamedTuple):
            """Define a subtest."""

            filename: str
            view_type: FileViewTypes
            content_type: str
            size: int = 1024

        sub_tests = [
            SubTest(
                "build.changes", FileViewTypes.TEXT, "text/plain; charset=utf-8"
            ),
            SubTest(
                "file.log", FileViewTypes.TEXT, "text/plain; charset=utf-8"
            ),
            SubTest(
                "file.txt", FileViewTypes.TEXT, "text/plain; charset=utf-8"
            ),
            SubTest(
                "hello.build", FileViewTypes.TEXT, "text/plain; charset=utf-8"
            ),
            SubTest(
                "hello.buildinfo",
                FileViewTypes.TEXT,
                "text/plain; charset=utf-8",
            ),
            SubTest(
                "file.sources", FileViewTypes.TEXT, "text/plain; charset=utf-8"
            ),
            SubTest(
                "readme.md", FileViewTypes.TEXT, "text/markdown; charset=utf-8"
            ),
            SubTest("a.out", FileViewTypes.BINARY, "application/octet-stream"),
            SubTest(
                "big.bin",
                FileViewTypes.TOO_BIG,
                "application/octet-stream",
                2**32,
            ),
        ]

        for sub_test in sub_tests:
            with self.subTest(sub_test.filename):
                file_in_artifact = FileInArtifact(path=sub_test.filename)
                file_in_artifact.file = File(size=sub_test.size)
                fileui = FileUI.from_file_in_artifact(file_in_artifact)
                self.assertEqual(fileui.content_type, sub_test.content_type)
                self.assertEqual(fileui.view_type, sub_test.view_type)

    def test_compressed(self) -> None:
        """Test from_file_in_artifact with a compressed file."""
        file_in_artifact = FileInArtifact(path="file.md.gz")
        file_in_artifact.file = File(size=123)
        fileui = FileUI.from_file_in_artifact(file_in_artifact)
        self.assertEqual(fileui.content_type, "application/gzip")
        self.assertEqual(fileui.view_type, "binary")


class PathMixinTests(ViewTestMixin, unittest.TestCase):
    """Tests for the ArtifactDetailView class."""

    def test_normalize_path(self) -> None:
        """Test PathMixin.normalize_path."""
        f = PathMixin.normalize_path
        self.assertEqual(f(""), "/")
        self.assertEqual(f("/"), "/")
        self.assertEqual(f("."), "/")
        self.assertEqual(f(".."), "/")
        self.assertEqual(f("../"), "/")
        self.assertEqual(f("../../.././../../"), "/")
        self.assertEqual(f("src/"), "/src/")
        self.assertEqual(f("src/.."), "/")
        self.assertEqual(f("/a/b/../c/./d//e/f/../g/"), "/a/c/d/e/g/")

    def test_path(self) -> None:
        """Test PathMixin.path."""
        view = self.instantiate_view_class(PathMixin, "/")
        self.assertEqual(view.path, "")

        view = self.instantiate_view_class(
            PathMixin, "/", path="/foo/bar/../baz"
        )
        self.assertEqual(view.path, "foo/baz/")


class FileDownloadMixinTests(ViewTestMixin, TestCase):
    """Test FileDownloadMixin."""

    playground_memory_file_store = False

    contents: ClassVar[dict[str, bytes]]
    empty: ClassVar[FileInArtifact]
    file: ClassVar[FileInArtifact]

    @classmethod
    def setUpTestData(cls) -> None:
        """Set up the common test fixture."""
        super().setUpTestData()
        cls.contents = {
            "empty.bin": b"",
            "file.md": bytes(range(256)),
        }
        artifact, _ = cls.playground.create_artifact(
            paths=cls.contents,
            create_files=True,
        )
        cls.empty = artifact.fileinartifact_set.get(path="empty.bin")
        cls.file = artifact.fileinartifact_set.get(path="file.md")

    def get_stream_response(
        self,
        file_in_artifact: FileInArtifact,
        range_header: tuple[int, int] | str | None = None,
        download: bool = True,
    ) -> HttpResponseBase:
        """Instantiate the view and get a streaming file response."""
        request = self.make_request("/")
        headers = {}
        match range_header:
            case str():
                headers["Range"] = range_header
            case [range_start, range_end]:
                headers["Range"] = f"bytes={range_start}-{range_end}"
        if headers:
            request.headers = cast(
                HttpHeaders,
                CaseInsensitiveMapping({**request.headers, **headers}),
            )

        ui_info = FileUI.from_file_in_artifact(file_in_artifact)
        view = self.instantiate_view_class(FileDownloadMixin, request)
        response = view.stream_file(file_in_artifact, ui_info, download)
        if isinstance(response, ProblemResponse):
            # This is needed by assertResponseProblem
            setattr(
                response,
                "json",
                lambda: json.loads(response.content.decode(response.charset)),
            )

        return response

    def assertFileResponse(
        self,
        response: HttpResponseBase,
        file_in_artifact: FileInArtifact,
        range_start: int = 0,
        range_end: int | None = None,
        disposition: str = "attachment",
    ):
        """Assert that response has the expected headers and content."""
        if range_start == 0 and range_end is None:
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        else:
            self.assertEqual(
                response.status_code, status.HTTP_206_PARTIAL_CONTENT
            )

        file_contents = self.contents[file_in_artifact.path]
        if range_end is None:
            range_end = len(file_contents) - 1
        expected_contents = file_contents[range_start : range_end + 1]

        headers = response.headers
        self.assertEqual(headers["Accept-Ranges"], "bytes")
        self.assertEqual(headers["Content-Length"], str(len(expected_contents)))

        if len(expected_contents) > 0:
            self.assertEqual(
                headers["Content-Range"],
                f"bytes {range_start}-{range_end}/{len(file_contents)}",
            )

        filename = os.path.basename(file_in_artifact.path)
        self.assertEqual(
            headers["Content-Disposition"],
            f'{disposition}; filename="{filename}"',
        )

        streaming_content = getattr(response, "streaming_content", None)
        assert streaming_content is not None
        self.assertEqual(b"".join(streaming_content), expected_contents)

    def test_get_file(self):
        """Get return the file."""
        response = self.get_stream_response(self.file)
        self.assertFileResponse(response, self.file)
        self.assertEqual(
            response.headers["content-type"], "text/markdown; charset=utf-8"
        )

    def test_get_file_inline(self):
        """Get return the file."""
        response = self.get_stream_response(self.file, download=False)
        self.assertFileResponse(response, self.file, disposition="inline")
        self.assertEqual(
            response.headers["content-type"], "text/markdown; charset=utf-8"
        )

    def test_get_empty_file(self):
        """Test empty downloadable file (which mmap doesn't support)."""
        response = self.get_stream_response(self.empty)
        self.assertFileResponse(response, self.empty)
        self.assertEqual(
            response.headers["content-type"],
            "application/octet-stream",
        )

    def test_get_file_range(self):
        """Get return part of the file (based on Range header)."""
        start, end = 10, 20
        response = self.get_stream_response(
            self.file, range_header=(start, end)
        )
        self.assertFileResponse(response, self.file, start, end)

    def test_get_file_content_range_to_end_of_file(self):
        """Server returns a file from a position to the end."""
        start, end = 5, len(self.contents["file.md"]) - 1
        response = self.get_stream_response(
            self.file, range_header=(start, end)
        )
        self.assertFileResponse(response, self.file, start, end)

    def test_get_file_content_range_invalid(self):
        """Get return an error: Range header was invalid."""
        invalid_range_header = "invalid-range"
        response = self.get_stream_response(
            self.file, range_header=invalid_range_header
        )
        self.assertResponseProblem(
            response, f'Invalid Range header: "{invalid_range_header}"'
        )

    def test_get_file_range_start_greater_file_size(self):
        """Get return 400: client requested an invalid start position."""
        file_size = len(self.contents["file.md"])
        start, end = file_size + 10, file_size + 20
        response = self.get_stream_response(
            self.file, range_header=(start, end)
        )
        self.assertResponseProblem(
            response,
            f"Invalid Content-Range start: {start}. " f"File size: {file_size}",
        )

    def test_get_file_range_end_is_file_size(self):
        """Get return 400: client requested and invalid end position."""
        end = len(self.contents["file.md"])
        response = self.get_stream_response(self.file, range_header=(0, end))
        self.assertResponseProblem(
            response,
            f"Invalid Content-Range end: {end}. File size: {end}",
        )

    def test_get_file_range_end_greater_file_size(self):
        """Get return 400: client requested an invalid end position."""
        file_size = len(self.contents["file.md"])
        end = file_size + 10
        response = self.get_stream_response(self.file, range_header=(0, end))
        self.assertResponseProblem(
            response,
            f"Invalid Content-Range end: {end}. " f"File size: {file_size}",
        )

    def test_get_file_url_redirect(self):
        """
        Get file response: redirect if get_url for the file is available.

        This would happen if the file is stored in a FileStore supporting
        get_url (e.g. an object storage) instead of being served from the
        server's file system.
        """
        destination_url = "https://some-backend.net/file?token=asdf"

        file_backend_mocked = mock.create_autospec(spec=FileBackendInterface)
        file_backend_mocked.get_url.return_value = destination_url

        with mock.patch.object(
            FileStore,
            "get_backend_object",
            autospec=True,
            return_value=file_backend_mocked,
        ):
            response = self.get_stream_response(self.file)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            self.assertEqual(response.url, destination_url)


class FileViewTests(ViewTestMixin, TestCase):
    """Test FileView."""

    contents: ClassVar[dict[str, bytes]]
    binary: ClassVar[FileInArtifact]
    dsc: ClassVar[FileInArtifact]
    empty: ClassVar[FileInArtifact]
    large: ClassVar[FileInArtifact]
    text: ClassVar[FileInArtifact]

    @classmethod
    def setUpTestData(cls) -> None:
        """Set up the common test fixture."""
        super().setUpTestData()
        cls.contents = {
            "file.md": b"# Title\nText",
            "file.dsc": b"Source: hello",
            "empty.bin": b"",
            "file.bin": bytes(range(256)),
            "largefile.bin": b"large",
        }
        artifact, _ = cls.playground.create_artifact(
            paths=cls.contents,
            create_files=True,
        )
        cls.empty = artifact.fileinartifact_set.get(path="empty.bin")
        cls.dsc = artifact.fileinartifact_set.get(path="file.dsc")
        cls.text = artifact.fileinartifact_set.get(path="file.md")
        cls.binary = artifact.fileinartifact_set.get(path="file.bin")
        cls.large = artifact.fileinartifact_set.get(path="largefile.bin")
        cls.large.file.size = 2**32
        cls.large.file.save()

    def get_view(
        self, file_in_artifact: FileInArtifact | None
    ) -> type[TestableFileView]:
        """Get a FileView for file_in_artifact."""

        class _ConcreteFileView(TestableFileView):
            def get_file_in_artifact(self) -> FileInArtifact | None:
                return file_in_artifact

        return _ConcreteFileView

    def test_abstract(self) -> None:
        """Ensure an unspecialised class raises ImproperlyConfigured."""
        view = FileView()
        with self.assertRaisesRegex(
            ImproperlyConfigured,
            "please provide a get_file_in_artifact implementation",
        ):
            view.get_file_in_artifact()

        with self.assertRaisesRegex(
            ImproperlyConfigured,
            "please provide a get_file_in_artifact implementation",
        ):
            view.file_in_artifact

    def test_getters_fileinartifact(self) -> None:
        """Test view getters with a FileInArtifact."""
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(self.text), request)
        self.assertEqual(view.get_file_in_artifact(), self.text)
        self.assertEqual(view.file_in_artifact, self.text)

        file_ui = FileUI.from_file_in_artifact(self.text)
        self.assertEqual(view.get_file_ui(), file_ui)
        self.assertEqual(view.file_ui, file_ui)

    def test_getters_none(self) -> None:
        """Test view getters with no FileInArtifact."""
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(None), request)
        self.assertIsNone(view.get_file_in_artifact())
        self.assertIsNone(view.file_in_artifact)
        self.assertIsNone(view.get_file_ui())
        self.assertIsNone(view.file_ui)

    def test_context_data_text(self) -> None:
        """Test context for text files."""
        file = self.text
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(file), request)
        context = view.get_context_data()

        file_linenumbers = context.pop("file_linenumbers")
        self.assertIn("L1", file_linenumbers)
        file_content = context.pop("file_content")
        self.assertIn("# Title", file_content)

        self.assertEqual(
            context,
            {
                "view": view,
                "file_in_artifact": file,
                "file_ui": FileUI.from_file_in_artifact(file),
                "file_template": "web/_file_text.html",
            },
        )

    def test_context_data_dsc(self) -> None:
        """Test context for text files."""
        file = self.dsc
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(file), request)
        context = view.get_context_data()

        file_linenumbers = context.pop("file_linenumbers")
        self.assertIn("L1", file_linenumbers)
        file_content = context.pop("file_content")
        self.assertIn('<span class="k">Source</span>', file_content)

        self.assertEqual(
            context,
            {
                "view": view,
                "file_in_artifact": file,
                "file_ui": FileUI.from_file_in_artifact(file),
                "file_template": "web/_file_text.html",
            },
        )

    def test_context_data_empty(self) -> None:
        """Test context for empty binary files."""
        file = self.empty
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(file), request)
        self.assertEqual(
            view.get_context_data(),
            {
                "view": view,
                "file_in_artifact": file,
                "file_ui": FileUI.from_file_in_artifact(file),
                "file_template": "web/_file_binary.html",
            },
        )

    def test_context_data_binary(self) -> None:
        """Test context for binary files."""
        file = self.binary
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(file), request)
        self.assertEqual(
            view.get_context_data(),
            {
                "view": view,
                "file_in_artifact": file,
                "file_ui": FileUI.from_file_in_artifact(file),
                "file_template": "web/_file_binary.html",
            },
        )

    def test_context_data_large(self) -> None:
        """Test context for files too large."""
        file = self.large
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(file), request)
        self.assertEqual(
            view.get_context_data(),
            {
                "view": view,
                "file_in_artifact": file,
                "file_ui": FileUI.from_file_in_artifact(file),
                "file_template": "web/_file_too_big.html",
                "file_max_size": MAX_FILE_SIZE,
            },
        )

    def test_context_data_none(self) -> None:
        """Test context with no file."""
        request = self.make_request("/")
        view = self.instantiate_view_class(self.get_view(None), request)
        self.assertEqual(
            view.get_context_data(),
            {
                "view": view,
            },
        )
