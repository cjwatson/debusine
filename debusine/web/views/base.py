# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Base infrastructure for web views."""

from typing import TYPE_CHECKING

from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)
from django.views.generic.edit import FormMixin

if TYPE_CHECKING:
    CreateViewBase = CreateView
    DeleteViewBase = DeleteView
    DetailViewBase = DetailView
    ListViewBase = ListView
    UpdateViewBase = UpdateView
    FormMixinBase = FormMixin
else:
    # Django's generic views don't support generic types at run-time yet.
    class _CreateViewBase:
        def __class_getitem__(*args):
            return CreateView

    class _DeleteViewBase:
        def __class_getitem__(*args):
            return DeleteView

    class _DetailViewBase:
        def __class_getitem__(*args):
            return DetailView

    class _ListViewBase:
        def __class_getitem__(*args):
            return ListView

    class _UpdateViewBase:
        def __class_getitem__(*args):
            return UpdateView

    class _FormMixinBase:
        def __class_getitem__(*args):
            return FormMixin

    CreateViewBase = _CreateViewBase
    DeleteViewBase = _DeleteViewBase
    DetailViewBase = _DetailViewBase
    ListViewBase = _ListViewBase
    UpdateViewBase = _UpdateViewBase
    FormMixinBase = _FormMixinBase
