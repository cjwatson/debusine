# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine WorkRequest view."""

import functools
from typing import Any

from django.contrib import messages
from django.core.exceptions import ValidationError
from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic.base import View

import yaml

from debusine.db.models import Artifact, WorkRequest
from debusine.db.models.work_requests import CannotRetry
from debusine.server.views import IsUserAuthenticated, ValidatePermissionsMixin
from debusine.tasks import TaskConfigError
from debusine.tasks.models import TaskTypes
from debusine.web.forms import WorkRequestForm
from debusine.web.views.base import (
    CreateViewBase,
    DetailViewBase,
    ListViewBase,
)
from debusine.web.views.mixins import PaginationMixin
from debusine.web.views.ui_shortcuts import UIShortcut, UIShortcutsView


class WorkRequestDumper(yaml.SafeDumper):
    """A YAML dumper that represents multi-line strings in the literal style."""

    def represent_scalar(self, tag, value, style=None):
        """Represent multi-line strings in the literal style."""
        if style is None and "\n" in value:
            style = "|"
        return super().represent_scalar(tag, value, style=style)


class WorkRequestDetailView(UIShortcutsView, DetailViewBase[WorkRequest]):
    """List work requests."""

    model = WorkRequest
    context_object_name = "work_request"
    default_template_name = "web/work_request-detail.html"

    @functools.cached_property
    def _validation_error_message(self) -> str | None:
        """If the work request fails validation, return a suitable message."""
        try:
            self.object.full_clean()
        except ValidationError as e:
            return str(e)
        else:
            return None

    def _current_view_is_specialized(self) -> bool:
        """
        Specialized (based on a plugin) view will be served.

        User did not force the default view, a plugin exists, and the work
        request passes validation.
        """
        use_specialized = self.request.GET.get("view", "default") != "generic"
        plugin_class = WorkRequestPlugin.plugin_for(self.object.task_name)

        return (
            use_specialized
            and plugin_class is not None
            and self._validation_error_message is None
        )

    def get_main_ui_shortcuts(self) -> list[UIShortcut]:
        """Return a list of UI shortcuts for this view."""
        actions = super().get_main_ui_shortcuts()
        if self.request.user.is_authenticated and self.object.can_retry():
            actions.append(UIShortcut.create_work_request_retry(self.object))
        return actions

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """
        Add context to the default DetailView context.

        Add the artifacts related to the work request.
        """
        context = super().get_context_data(**kwargs)

        plugin_view = WorkRequestPlugin.plugin_for(self.object.task_name)

        if plugin_view is not None and self._current_view_is_specialized():
            return {**context, **plugin_view().get_context_data(self.object)}

        if self._validation_error_message is not None:
            context["validation_error"] = self._validation_error_message
        elif plugin_view:
            # plugin_view for WorkRequest.task_name exists, but the
            # response will return the generic WorkRequest view
            context["specialized_view_path"] = self.request.path

        try:
            task = self.object.get_task()
        except TaskConfigError:
            task = None
        try:
            if task and (
                source_artifact_ids := task.get_source_artifacts_ids()
            ):
                context["source_artifacts"] = Artifact.objects.filter(
                    pk__in=source_artifact_ids
                )
        except NotImplementedError:
            # TODO: remove this once get_source_artifacts_ids has been
            # implemented for all artifact types
            context["source_artifacts_not_implemented"] = True

        artifacts = list(
            Artifact.objects.filter(
                created_by_work_request=self.object
            ).order_by("category", "id")
        )

        # Generate UI shortcuts for artifacts
        for artifact in artifacts:
            actions: list[UIShortcut] = [
                UIShortcut.create_artifact_view(artifact),
                UIShortcut.create_artifact_download(artifact),
            ]
            self.add_object_shortcuts(artifact, *actions)

        context["artifacts"] = artifacts
        context["task_data"] = yaml.dump(
            self.object.task_data, Dumper=WorkRequestDumper
        )
        return context

    def get_template_names(self):
        """Return the plugin's template_name or the default one."""
        if self._current_view_is_specialized():
            return WorkRequestPlugin.plugin_for(
                self.object.task_name
            ).template_name

        return self.default_template_name


class WorkRequestPlugin:
    """
    WorkRequests with specific outputs must subclass it.

    When subclassing, the subclass:
    - Is automatically used by the /work-request/ID/ endpoints
    - Must define the "template_name" and "task_name"
    - Must implement "get_context_data(work_request)"
    """

    _work_request_plugins: dict[str, type["WorkRequestPlugin"]] = {}

    def __init_subclass__(cls, **kwargs):  # noqa: U100
        """Register the plugin."""
        cls._work_request_plugins[cls.task_name] = cls

    @classmethod
    def plugin_for(cls, task_name: str) -> type["WorkRequestPlugin"] | None:
        """Return WorkRequestPlugin for task_name or None."""
        return cls._work_request_plugins.get(task_name)

    def get_context_data(self, work_request: WorkRequest) -> dict[str, Any]:
        """Must be implemented by subclasses."""
        raise NotImplementedError()


class WorkRequestListView(PaginationMixin, ListViewBase[WorkRequest]):
    """List work requests."""

    model = WorkRequest
    template_name = "web/work_request-list.html"
    context_object_name = "work_request_list"
    paginate_by = 50

    def get_queryset(self):
        """Filter work requests displayed by the workspace GET parameter."""
        queryset = super().get_queryset().exclude(task_type=TaskTypes.INTERNAL)

        workspace_name = self.request.GET.get("workspace")
        if workspace_name is not None:
            queryset = queryset.filter(workspace__name=workspace_name)

        if not self.request.user.is_authenticated:
            # Non-authenticated users can only list WorkRequests in
            # a public workspace
            queryset = queryset.filter(workspace__public=True)

        return queryset

    def get_ordering(self):
        """Return field used for sorting."""
        order = self.request.GET.get("order")
        if order in ("id", "created_at", "task_name", "status", "result"):
            if self.request.GET["asc"] == "0":
                return "-" + order
            else:
                return order

        return "-created_at"

    def get_context_data(self, **kwargs):
        """Add context to the default ListView data."""
        context = super().get_context_data(**kwargs)

        context["order"] = self.get_ordering().removeprefix("-")
        context["asc"] = self.request.GET.get("asc", "0")

        if workspace := self.request.GET.get("workspace"):
            context["workspace"] = workspace

        return context


class WorkRequestCreateView(
    ValidatePermissionsMixin, CreateViewBase[WorkRequest, WorkRequestForm]
):
    """Form view for creating a work request."""

    model = WorkRequest
    template_name = "web/work_request-create.html"
    form_class = WorkRequestForm

    permission_denied_message = (
        "You need to be authenticated to create a Work Request"
    )
    permission_classes = [IsUserAuthenticated]

    def get_form_kwargs(self) -> dict[str, Any]:
        """Extend the default kwarg arguments: add "user"."""
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def get_success_url(self):
        """Redirect to work_requests:detail for the created WorkRequest."""
        return reverse("work_requests:detail", kwargs={"pk": self.object.id})


class WorkRequestRetryView(ValidatePermissionsMixin, View):
    """Form view for retrying a work request."""

    permission_denied_message = (
        "You need to be authenticated to retry a Work Request"
    )
    permission_classes = [IsUserAuthenticated]

    @functools.cached_property
    def work_request(self) -> WorkRequest:
        """Return the work request for this request."""
        try:
            return WorkRequest.objects.get(pk=self.kwargs["pk"])
        except WorkRequest.DoesNotExist:
            raise Http404(f"Work request {self.kwargs['pk']} not found")

    def post(
        self, request: HttpRequest, *args, **kwargs  # noqa: U100
    ) -> HttpResponse:
        """Handle POST requests."""
        try:
            new_work_request = self.work_request.retry()
        except CannotRetry as e:
            messages.error(self.request, f"Cannot retry: {e}")
            return redirect("work_requests:detail", pk=self.work_request.pk)

        return redirect("work_requests:detail", pk=new_work_request.pk)
