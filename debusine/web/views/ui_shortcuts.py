# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
View extension to add context-dependent UI shortcut widgets.

A UI shortcut is a widget that can be rendered associated to a UI element to
provide a shortcut to commonly used views or actions related to it.
"""

from typing import NamedTuple, Self

from django.db.models import Model
from django.template.backends.utils import csrf_input
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import SafeString
from django.views.generic.base import ContextMixin

from debusine.artifacts.models import ArtifactCategory
from debusine.db.models import Artifact, FileInArtifact, WorkRequest
from debusine.db.models.artifacts import (
    ARTIFACT_CATEGORY_ICON_NAMES,
    ARTIFACT_CATEGORY_SHORT_NAMES,
)


class UIShortcut(NamedTuple):
    """Renderable UI shortcut."""

    #: User-readable label
    label: str
    #: Icon (name in the Bootstrap icon set, without the leading "bi-")
    icon: str
    #: Target URL for the action
    url: str

    def render(self, context) -> SafeString:  # noqa: U100
        """Render the shortcut as an <a> button."""
        return format_html(
            "<a class='btn btn-outline-secondary'"
            " href='{url}' title='{label}'>"
            "<span class='bi bi-{icon}'></span>"
            "</a>",
            label=self.label,
            icon=self.icon,
            url=self.url,
        )

    @classmethod
    def create_work_request_view(cls, work_request: WorkRequest) -> Self:
        """Create a shortcut to view a work request."""
        return cls(
            label="View work request",
            icon="hammer",
            url=reverse("work_requests:detail", kwargs={"pk": work_request.pk}),
        )

    @classmethod
    def create_work_request_retry(
        cls, work_request: WorkRequest
    ) -> "UIShortcut":
        """Create a shortcut to retry a work request."""
        return UIShortcutPOST(
            label="Retry work request",
            icon="arrow-repeat",
            url=reverse("work_requests:retry", kwargs={"pk": work_request.pk}),
        )

    @classmethod
    def create_artifact_view(cls, artifact: Artifact) -> Self:
        """Create a shortcut to view an artifact."""
        category = ArtifactCategory(artifact.category)
        short_name = ARTIFACT_CATEGORY_SHORT_NAMES.get(category, "artifact")
        return cls(
            label=f"View {short_name} artifact",
            icon=ARTIFACT_CATEGORY_ICON_NAMES.get(category, "folder"),
            url=reverse(
                "artifacts:detail", kwargs={"artifact_id": artifact.pk}
            ),
        )

    @classmethod
    def create_file_view(cls, file_in_artifact: FileInArtifact) -> Self:
        """Create a shortcut to view a file."""
        return cls(
            label=f"View {file_in_artifact.path}",
            icon="file-earmark",
            url=reverse(
                "artifacts:detail-file",
                kwargs={
                    "artifact_id": file_in_artifact.artifact_id,
                    "file_in_artifact_id": file_in_artifact.id,
                    "path": file_in_artifact.path,
                },
            ),
        )

    @classmethod
    def create_file_view_raw(cls, file_in_artifact: FileInArtifact) -> Self:
        """Create a shortcut to stream a file inline."""
        return cls(
            label=f"View {file_in_artifact.path} raw",
            icon="file-earmark-code",
            url=reverse(
                "artifacts:detail-file-raw",
                kwargs={
                    "artifact_id": file_in_artifact.artifact_id,
                    "file_in_artifact_id": file_in_artifact.id,
                    "path": file_in_artifact.path,
                },
            ),
        )

    @classmethod
    def create_file_download(cls, file_in_artifact: FileInArtifact) -> Self:
        """Create a shortcut to download a file."""
        return cls(
            label=f"Download {file_in_artifact.path}",
            icon="file-earmark-arrow-down",
            url=reverse(
                "artifacts:download-path",
                kwargs={
                    "artifact_id": file_in_artifact.artifact_id,
                    "path": file_in_artifact.path,
                },
            ),
        )

    @classmethod
    def create_artifact_download(cls, artifact: Artifact) -> Self:
        """Create a shortcut to download an artifact."""
        return cls(
            label="Download artifact",
            icon="download",
            url=reverse(
                "artifacts:download", kwargs={"artifact_id": artifact.pk}
            )
            + "?archive=tar.gz",
        )


class UIShortcutPOST(UIShortcut):
    """UI shortcut that triggers a POST."""

    def render(self, context) -> SafeString:
        """Render the shortcut as a form."""
        return format_html(
            "<form method='post' action='{url}'>{csrf}"
            "<button type='submit' class='btn btn-primary bi bi-{icon}'"
            " title='{label}'></button>"
            "</form>",
            csrf=csrf_input(context["request"]),
            label=self.label,
            icon=self.icon,
            url=self.url,
        )


class UIShortcutsView(ContextMixin):
    """View mixin to contribute shortcuts to template contexts."""

    def get_main_ui_shortcuts(self) -> list[UIShortcut]:
        """Return a list of shortcuts for this view."""
        return []

    def add_object_shortcuts(self, obj: Model, *actions: UIShortcut) -> None:
        """
        Store a shortcut for an object.

        This allows computing object-specific shortcuts when it's possible to
        do it in a database-efficient way, and stores them to be looked up in a
        way that is convenient for the templates.
        """
        stored = getattr(obj, "_ui_shortcuts", None)
        if stored is None:
            stored = []
            setattr(obj, "_ui_shortcuts", stored)
        stored.extend(actions)

    def get_context_data(self, **kwargs):
        """Add main shortcuts to the template context."""
        context = super().get_context_data(**kwargs)
        context["main_ui_shortcuts"] = self.get_main_ui_shortcuts()
        return context
